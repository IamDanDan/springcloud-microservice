# springcloud-microservice
Spring Cloud 快速搭建微服务
<br>
spring cloud 为开发人员提供了快速构建分布式系统的一些工具，包括配置管理、服务发现、断路器、路由、微代理、事件总线、全局锁、决策竞选、分布式会话等等，下面进行个服务分部搭建源码
<br>


##组织结构

	springcloud-microservice
	├── micro-eureka-server -- eureka 服务注册中心      [端口 8101]
	| 
	├── micro-service-example -- 提供服务
	|	├── micro-serviceA -- 服务提供者A[端口 8201] 
	|	├── micro-serviceB -- 服务提供者B[端口 8202] 
	|  
	├── micro-ribbon-client --服务消费者（rest+ribbon） [端口 8301] 
    |	
	|── micro-feign-client  --服务消费者（feign）  [端口 8302] 


### [eureka 服务注册中心      [端口 8101]](http://git.oschina.net/lishangzhi2012/springcloud-microservice/blob/master/micro-eureka-server/README.md "micro-eureka-server")
### [提供服务](http://git.oschina.net/lishangzhi2012/springcloud-microservice/tree/master/micro-service-example)
### [服务消费者（rest+ribbon） [端口 8301] ](http://git.oschina.net/lishangzhi2012/springcloud-microservice/tree/master/micro-ribbon-client)
### [服务消费者（feign）  [端口 8302] ](http://git.oschina.net/lishangzhi2012/springcloud-microservice/tree/master/micro-feign-client)




<br>
<br>
### 持续更新！！！





