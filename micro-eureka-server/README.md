# micro-eureka-server
服务的注册与发现（Eureka）

###服务注册中心
Spring Cloud Netflix的Eureka ,eureka是一个服务注册和发现模块

eureka是一个高可用的组件，它没有后端缓存，每一个实例注册之后需要向注册中心发送心跳（因此可以在内存中完成），在默认情况下erureka server也是一个eureka client ,必须要指定一个 server。eureka server的配置文件

    ############################################################################
    ###################### eureka 注册中心 #######################
    ############################################################################
    
    spring:
      application:
    name: EurekaServer
    
    server:
      port: 8101
    #  port: ${PORT:${SERVER_PORT:0}}
    
    security:
      basic:
    enabled: true # 启用认证
      user:
    name: lishangzhi
    password: 123456
    
    
    eureka:  
      instance: 
    hostname: localhost
      client: 
    registerWithEureka: false
    fetchRegistry: false   
    serviceUrl: 
      defaultZone: http://lishangzhi:123456@localhost:8101/eureka/

    


启动工程,打开浏览器访问：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0830/093259_30827144_1468963.png "eureka.png")
后端日志：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0830/093310_ad83815a_1468963.png "eureka-server.png")

##注：
注册中心启动服务认证,调用时需进行认证（此坑掉下去很久才爬出来~囧）