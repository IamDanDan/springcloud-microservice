package org.wanma.example.springcloud.clientfeign.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "SERVICEHELLOA")
public interface HelloAFeignService {

	@RequestMapping(value = "/HelloA",method = RequestMethod.GET)
    String visitHelloA(@RequestParam(value = "name") String name);

}
