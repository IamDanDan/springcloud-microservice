package org.wanma.example.springcloud.clientfeign.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.wanma.example.springcloud.clientfeign.service.HelloAFeignService;

@RestController
public class HelloAFeignController {

    @Autowired
    HelloAFeignService feignService;
    
    @RequestMapping(value = "/HelloAFeign")
    public String visitHelloA(@RequestParam String name){
        return feignService.visitHelloA(name);
    }
    
}