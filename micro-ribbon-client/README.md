# micro-service-ribbon
服务的注册与发现（ribbon）

###服务注册中心
Ribbon是Netflix发布的云中间层服务开源项目，主要功能是提供客户端负载均衡算法。Ribbon客户端组件提供一系列完善的配置项，如，连接超时，重试等。简单的说，Ribbon是一个客户端负载均衡器，我们可以在配置文件中列出load Balancer后面所有的机器，Ribbon会自动的帮助你基于某种规则(如简单轮询，随机连接等)去连接这些机器，我们也很容易使用Ribbon实现自定义的负载均衡算法。
    
    ############################################################################
    ###################### Ribbon#######################
    ############################################################################
    
    spring:
      application:
    name: ClientRibbon   
    
    server:
      port: 8301
    #  port: ${PORT:${SERVER_PORT:0}}
    
    eureka:
      client:
    serviceUrl:
      defaultZone: http://lishangzhi:123456@localhost:8101/eureka  #注册 中心已经开启认证
      instance:
    prefer-ip-address: true
    instanceId: ${spring.application.name}:${server.port} 

架构实施如图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0825/173007_b65f49e9_1468963.png "img.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0825/173013_4f16d3a8_1468963.png "2279594-9f10b702188a129d.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0830/095726_64c6b80e_1468963.png "micro-servicvea.png")


1. 一个服务注册中心，micro-eureka-server,端口为8101
1. micro-service工程跑了两个实例micro-serviceA、micro-serviceB，端口分别为8201,8202，分别向服务注册中心注册
1. micro-ribbon-client端口为8301,向服务注册中心注册
1. micro-ribbon-client通过restTemplate调用micro-service的HelloA接口时，因为用ribbon进行了负载均衡，会轮流的调用micro-service：8201和8202两个端口的HelloA接口；
